# Giant Virus Research

## Abstract

Beginning in 1990, viruses have been identified with hundreds of genes that equate to many Mb of data.  These cell sized nucleo-cytoplasmic large DNA viruses (NCLDV) come equipped with the necessary components for performing their own protein synthesis and challenge one ofthe central dogmas surrounding viral research; the idea that viruses are a “bags of genes” co-oped from host genomes.  However, phylogenic studies of giant viruses are complicated by genetic mosaicism, and frequent discoveries. As it stands, the current taxonomy published by the International Committee on Taxonomy of Viruses provides confidence up to the genus level classifications. Machine learning could hold the key to completing the taxonomy by uncovering new patterns in noisy data. One such approach will be called naive Bayesian Hierarchical Clustering, a machine learning technique leveraging conditional probability and Bayesian priors.  This approach will replicate the current published taxonomy of the order Caudovirales with new confidence and inspire further research with more advanced statistical methods for BHC.

## Research Objective 

To replicate the hierarchical assignments for the order Caudovirales from the ICTV using naive Bayesian Hierarchical Clustering and to confirm the results on a quantitative basis.  Results should be represented in Newick format and a dendrogram.
